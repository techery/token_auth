Gem::Specification.new do |s|
  s.name        = 'token_auth'
  s.version     = '0.0.1'
  s.date        = '2014-04-17'
  s.summary     = "Token-based authentication"
  s.description = "Token-based authentication gem"
  s.authors     = ["Anna Balina"]
  s.email       = 'ann.balina@gmail.com'
  s.files       = ["lib/token_auth.rb"]
  # s.homepage    =
  #   'http://google.com'
  s.license       = 'MIT'
end